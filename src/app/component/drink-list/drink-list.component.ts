import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Drink } from 'src/app/model/Drink';
import { DrinksService } from 'src/app/service/drinks.service';


@Component({
  selector: 'app-drink-list',
  templateUrl: './drink-list.component.html',
  styleUrls: ['./drink-list.component.scss']
})
export class DrinkListComponent implements OnInit {

  drinks : Observable<Drink[]>;
  
  constructor(private drinkService : DrinksService) { }

  ngOnInit(): void {
    this.drinks = this.drinkService.getDrinks();
  }

}
