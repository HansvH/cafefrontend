import { Drink } from "../model/Drink";

const DRINKS : Drink[]=
[
    {
        "name": "Tea-",
        "quantity": 200,
        "type": "Hot"
    },
    {
        "name": "Espresso-",
        "quantity": 50,
        "type": "Hot"
    }
]

export {DRINKS}